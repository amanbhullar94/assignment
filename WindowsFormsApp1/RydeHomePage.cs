﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//NAVNEET KAUR BHULLAR (c0735315), VINAY KUMAR MYAKALA (c0736282), JASPREET SINGH (c0735429)

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            textBox1.Focus();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string rydeType = "Ryde Type";
            string from = textBox1.Text;
            string to = textBox2.Text;
            double baseFare = 2.50;
            double chargesPerKm = 0.81;
            double distanceCharge = 0;
            double serviceFees = 1.75;
            double totalPrice = 0;
            double minimumFare = 5.50;

            DateTime currentTime = DateTime.Now;

            if ((Convert.ToDateTime("10:00:00 AM") <= currentTime && Convert.ToDateTime("12:00:00 PM") >= currentTime) ||
                (Convert.ToDateTime("04:00:00 PM") <= currentTime && Convert.ToDateTime("06:00:00 PM") >= currentTime) ||
                (Convert.ToDateTime("08:00:00 PM") <= currentTime && Convert.ToDateTime("09:00:00 PM") >= currentTime))
            {
                chargesPerKm += chargesPerKm * 0.2;
            }

            if (from == "275 Yorkland Blvd" && to == "CN Tower")
            {
                if (radioButton1.Checked == false && radioButton2.Checked == false)
                {
                    MessageBox.Show("Please select either Ryde Pool or Ryde Direct.");
                }
                else
                {
                    if (radioButton1.Checked == true)
                        rydeType = "Ryde Pool";
                    else
                    {
                        rydeType = "Ryde Direct";
                        baseFare += Math.Round((baseFare / 10), 2);
                        chargesPerKm += Math.Round((chargesPerKm * 15 / 100), 2);
                    }


                    double kmTravelled = 22.9;
                    distanceCharge = Math.Round((kmTravelled * chargesPerKm), 2);
                    totalPrice = Math.Round(((baseFare) + (distanceCharge) + (serviceFees)), 2);

                    if (totalPrice < minimumFare)
                        totalPrice = minimumFare;

                    Form2 screen2 = new Form2(rydeType, from, to, baseFare, distanceCharge, serviceFees, totalPrice);
                    screen2.ShowDialog();
                    this.Hide();
                }
            }
            else if (from == "Fairview Mall" && to == "Tim Hortons")
            {
                if (radioButton1.Checked == false && radioButton2.Checked == false)
                {
                    MessageBox.Show("Please select either Ryde Pool or Ryde Direct.");
                }
                else
                {
                    if (radioButton1.Checked == true)
                        rydeType = "Ryde Pool";
                    else
                    {
                        rydeType = "Ryde Direct";
                        baseFare += Math.Round((baseFare / 10), 2);
                        serviceFees += Math.Round((serviceFees * 15 / 100), 2);
                    }


                    double kmTravelled = 1.2;
                    distanceCharge = Math.Round((kmTravelled * chargesPerKm), 2);
                    totalPrice = Math.Round(((baseFare) + (distanceCharge) + (serviceFees)), 2);

                    if (totalPrice < minimumFare)
                        totalPrice = minimumFare;

                    Form2 screen2 = new Form2(rydeType, from, to, baseFare, distanceCharge, serviceFees, totalPrice);
                    screen2.ShowDialog();
                    this.Hide();
                }
            }
            else if (from == "" || to == "")
            {
                MessageBox.Show("Please enter both To and From fields.");
            }
            else
            {
                MessageBox.Show("INVALID COMBINATION OF TO AND FROM!!!!");
            }

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
